//alert("Hello, B176!");

/*
	Objects 
		-is a data type that is used to represent real world objects
		-it is also a collection of related data and/or functionalities

	Syntax:
		let objectName = {
			keyNameA: valueA,
			keyNameB: valueB
		}
*/
/*
.1. Create a new set of pokemon for pokemon battle. (Same as our discussion)
    - Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
    (target.health - this.attack)

2. If health is below 5, invoke faint function.
*/


/*
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This Pokemon tackled targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	},
	faint: function() {
		console.log("Pokemon fainted")
	}
}
*/

//console.log(myPokemon)

function Pokemon(name, level) {
	//properties
	this.name = name;   
	this.level = level; //25
	this.health = 2 * level; //  2*5 = 10
	this.attack = level  //25    5  

	//methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to "
		 + (target.health -  this.attack));

		tHealth = target.health - this.attack

		//console.log(tHealth)
		//If health is below 5, invoke faint function.
		if(tHealth<5) {

			target.faint()
		}

	};//end tackle method

	//begin faint
	this.faint = function() {
		
		// if(this.health<5){
		console.log(this.name + " fainted.")
		// }	
		}
		//console.log(this.name + "fainted.")
	}

//provide values to the function parameters:
let pikachu = new Pokemon("Pikachu", 20)
let charizard = new Pokemon("Charizard", 15)

//faint
 let borax = new Pokemon("Borax", 26)
// let dragon = new Pokemon("Dragon", 5)

 // console.log(pikachu.tackle(charizard))
//console.log(charizard)


pikachu.tackle(charizard)

borax.tackle(charizard)
